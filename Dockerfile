FROM adoptopenjdk/openjdk11:alpine-jre

RUN addgroup -S groupapp && adduser -S userapp -G groupapp

USER userapp:groupapp

ARG JAR_FILE=target/bcp-ws-api-exchange-rate-0.0.1-SNAPSHOT.jar

COPY ${JAR_FILE} app.jar

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
