package com.bcp.ws.api.exchange.service;

import java.util.List;

import com.bcp.ws.api.exchange.dto.ExchangeRateDTO;

import rx.Single;

public interface ExchangeRateService {

	Single<List<ExchangeRateDTO>> findAll();

	Single<ExchangeRateDTO> get(Long id);

	Single<ExchangeRateDTO> create(ExchangeRateDTO exchangeRateDTO);

	Single<ExchangeRateDTO> update(Long id, ExchangeRateDTO exchangeRateDTO);
	
	Single<ExchangeRateDTO> calculateExchangeRate(ExchangeRateDTO exchangeRateDTO);
}
