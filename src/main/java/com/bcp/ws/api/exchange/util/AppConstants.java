package com.bcp.ws.api.exchange.util;

public interface AppConstants {

	String NOT_FOUND_KEY = "notFoundKey";
	
	String ENTITY_TABLE_EXCHANGE_RATE = "exchange_rate";
	
	String PARAM_REQUEST_ID = "id";
	String PARAM_AUTHORITIES = "authorities";
	
	String ERROR_TYPE = "";
	
	String USERNAME_ADMIN = "ADMIN";
	String PASSWORD_ADMIN = "ADMIN";
}
