package com.bcp.ws.api.exchange.exception;

import org.springframework.http.HttpStatus;

public class AppInternalException extends Exception {

	private static final long serialVersionUID = 1L;

	private HttpStatus statusCode;
	private String messageKey;
	private Object[] messageParameters;

	public AppInternalException() {
		super();
	}

	public AppInternalException(Throwable cause) {
		super(cause);
	}

	public AppInternalException(Throwable cause, HttpStatus statusCode, String messageKey) {
		this(cause);
		this.statusCode = statusCode;
		this.messageKey = messageKey;
	}

	public AppInternalException(Throwable cause, HttpStatus statusCode, String messageKey,
			Object[] messageParameters) {
		this(cause, statusCode, messageKey);
		this.messageParameters = messageParameters;
	}

	public AppInternalException(HttpStatus statusCode, String messageKey) {
		this.statusCode = statusCode;
		this.messageKey = messageKey;
	}

	public AppInternalException(HttpStatus statusCode, String messageKey,
			Object[] messageParameters) {
		this(statusCode, messageKey);
		this.messageParameters = messageParameters;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public String getMessageKey() {
		return messageKey;
	}

	public Object[] getMessageParameters() {
		return messageParameters;
	}
}
