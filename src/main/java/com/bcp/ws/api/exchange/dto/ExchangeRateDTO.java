package com.bcp.ws.api.exchange.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Data
public class ExchangeRateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	@NotBlank(message = "{fromCurrency.notempty}")
	private String fromCurrency;

	@NotBlank(message = "{toCurrency.notempty}")
	private String toCurrency;

	private BigDecimal amountExchangeRate;

	private BigDecimal fromAmount;

	private BigDecimal toAmount;
}
