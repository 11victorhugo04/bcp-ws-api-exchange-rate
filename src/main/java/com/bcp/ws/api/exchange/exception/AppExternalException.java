package com.bcp.ws.api.exchange.exception;

public class AppExternalException extends Exception {

	private static final long serialVersionUID = 1L;

	private String messageKey;
	private Object[] messageParameters;

	public AppExternalException() {
		super();
	}

	public AppExternalException(Throwable cause) {
		super(cause);
	}

	public AppExternalException(Throwable cause, String messageKey) {
		super(cause);
		this.messageKey = messageKey;
	}

	public AppExternalException(Throwable cause, String messageKey, Object[] messageParameters) {
		this(cause, messageKey);
		this.messageParameters = messageParameters;
	}
	
	public AppExternalException(String messageKey) {
		this.messageKey = messageKey;
	}

	public AppExternalException(String messageKey, Object[] messageParameters) {
		this(messageKey);
		this.messageParameters = messageParameters;
	}

	public String getMessageKey() {
		return messageKey;
	}

	public Object[] getMessageParameters() {
		return messageParameters;
	}
}
