package com.bcp.ws.api.exchange.exception;

import java.time.LocalDateTime;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bcp.ws.api.exchange.domain.error.ApiErrorResponse;
import com.bcp.ws.api.exchange.util.AppConfigs;
import com.bcp.ws.api.exchange.util.AppConstants;
import com.bcp.ws.api.exchange.util.AppMessages;

@ControllerAdvice
public class AdviceController extends ResponseEntityExceptionHandler {

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	protected Environment environment;

	@ExceptionHandler(AppInternalException.class)
	public ResponseEntity<Object> handleEntityNotFound(AppInternalException ex, WebRequest request) {

		ApiErrorResponse response = buildApiErrorResponse(ex.getStatusCode(), ex.getMessageKey());
		response.setDetail(ex.getLocalizedMessage());
		response.setPath(request.getDescription(true));
		return buildResponseEntity(response);
	}

	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		ApiErrorResponse response = buildApiErrorResponse(HttpStatus.BAD_REQUEST, AppMessages.GENERIC_ERROR);
		response.setDetail(ex.getLocalizedMessage());
		response.setPath(request.getDescription(true));
		return handleExceptionInternal(ex, response, headers, response.getStatus(), request);
	}

	protected ApiErrorResponse buildApiErrorResponse(HttpStatus code, String message) {
		ApiErrorResponse response = new ApiErrorResponse();
		response.setVersion(getValueConfig(AppConfigs.SPRING_APPLICATION_VERSION));
		response.setProfile(getValueConfig(AppConfigs.SPRING_PROFILE_ACTIVE));
		response.setStatus(code);
		response.setTimestamp(LocalDateTime.now());
		response.setMessage(getValueMessage(message));
		return response;
	}

	protected ResponseEntity<Object> buildResponseEntity(ApiErrorResponse apiErrorResponse) {
		return new ResponseEntity<>(apiErrorResponse, apiErrorResponse.getStatus());
	}

	protected String getValueMessage(String key) {
		return messageSource.getMessage(key, new Object[] {}, AppConstants.NOT_FOUND_KEY, Locale.getDefault());
	}

	protected String getValueConfig(String key) {
		return environment.getProperty(key, AppConstants.NOT_FOUND_KEY);
	}
}
