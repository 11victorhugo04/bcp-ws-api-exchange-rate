package com.bcp.ws.api.exchange.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.ws.api.exchange.domain.generic.ApiGenericResponse;
import com.bcp.ws.api.exchange.dto.ExchangeRateDTO;
import com.bcp.ws.api.exchange.exception.AppInternalException;
import com.bcp.ws.api.exchange.service.ExchangeRateService;
import com.bcp.ws.api.exchange.util.AppConstants;
import com.bcp.ws.api.exchange.util.AppMessages;
import com.bcp.ws.api.exchange.util.AppUrls;

import rx.Single;
import rx.schedulers.Schedulers;

@RequestMapping(AppUrls.API_EXCHANGE_RATE)
@RestController
public class ApiWsExchangeRateController extends ApiAbstractController {

	private ExchangeRateService exchangeRateService;

	@Autowired
	public void setExchangeRateService(ExchangeRateService exchangeRateService) {
		this.exchangeRateService = exchangeRateService;
	}

	@GetMapping(AppUrls.API_EXCGANGE_RATE_LIST)
	public Single<ApiGenericResponse<List<ExchangeRateDTO>>> list() {
		return exchangeRateService.findAll().subscribeOn(Schedulers.io())
				.map(e -> buildApiResponse(HttpStatus.OK, AppMessages.GENERIC_LIST_SUCCESS, e));
	}

	@GetMapping(AppUrls.API_EXCGANGE_RATE_SINGLE_ID)
	public Single<ApiGenericResponse<ExchangeRateDTO>> single(@PathVariable(AppConstants.PARAM_REQUEST_ID) Long id)
			throws AppInternalException {
		return exchangeRateService.get(id).subscribeOn(Schedulers.io())
				.map(e -> buildApiResponse(HttpStatus.OK, AppMessages.GENERIC_SINGLE_SUCCESS, e));
	}

	@PostMapping(AppUrls.API_EXCGANGE_RATE_SINGLE)
	public Single<ApiGenericResponse<ExchangeRateDTO>> create(@Valid @RequestBody ExchangeRateDTO exchangeRateDTO) {
		return exchangeRateService.create(exchangeRateDTO).subscribeOn(Schedulers.io())
				.map(e -> buildApiResponse(HttpStatus.OK, AppMessages.GENERIC_CREATE_SUCCESS, e));
	}

	@PutMapping(AppUrls.API_EXCGANGE_RATE_SINGLE_ID)
	public Single<ApiGenericResponse<ExchangeRateDTO>> update(@PathVariable(AppConstants.PARAM_REQUEST_ID) Long id,
			@Valid @RequestBody ExchangeRateDTO exchangeRateDTO) {
		return exchangeRateService.update(id, exchangeRateDTO).subscribeOn(Schedulers.io())
				.map(e -> buildApiResponse(HttpStatus.OK, AppMessages.GENERIC_UPDATE_SUCCESS, e));

	}
	
	@PostMapping(AppUrls.API_EXCGANGE_RATE_CALCULATE)
	public Single<ApiGenericResponse<ExchangeRateDTO>> calculate(@Valid @RequestBody ExchangeRateDTO exchangeRateDTO) {
		return exchangeRateService.calculateExchangeRate(exchangeRateDTO).subscribeOn(Schedulers.io())
				.map(e -> buildApiResponse(HttpStatus.OK, AppMessages.GENERIC_UPDATE_SUCCESS, e));

	}
}
