package com.bcp.ws.api.exchange.util;

public interface AppMessages {

    String GENERIC_SUCCESS = "message.generic.success";
    String GENERIC_FAILED = "message.generic.failed";
    String GENERIC_ERROR = "message.generic.error";
    String GENERIC_LIST_SUCCESS = "message.generic.list.success";
    String GENERIC_LIST_EMPTY = "message.generic.list.empty";
    String GENERIC_LIST_ERROR = "message.generic.list.error";
    String GENERIC_SINGLE_SUCCESS = "message.generic.single.success";
    String GENERIC_SINGLE_EMPTY = "message.generic.single.empty";
    String GENERIC_SINGLE_ERROR = "message.generic.single.error";
    String GENERIC_SAVE_SUCCESS = "message.generic.save.success";
    String GENERIC_SAVE_ERROR = "message.generic.save.error";
    String GENERIC_CREATE_SUCCESS = "message.generic.create.success";
    String GENERIC_CREATE_ERROR = "message.generic.create.error";
    String GENERIC_UPDATE_SUCCESS = "message.generic.update.success";
    String GENERIC_UPDATE_ERROR = "message.generic.update.error";
    String GENERIC_DELETE_SUCCESS = "message.generic.delete.success";
    String GENERIC_DELETE_ERROR = "message.generic.delete.error";
    
    String INVALID_CREDENTIALS = "message.invalid.credentials";
    String NOT_EXIST_EXCHANGE_RATE = "message.notExist.exchangeRate";
    String CALCULATE_EXCHANGE_RATE_SUCCESS = "message.calculate.exchangeRate.success";
}
