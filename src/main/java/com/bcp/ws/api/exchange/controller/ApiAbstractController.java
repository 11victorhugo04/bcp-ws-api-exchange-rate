package com.bcp.ws.api.exchange.controller;

import java.time.LocalDateTime;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;

import com.bcp.ws.api.exchange.domain.generic.ApiGenericResponse;
import com.bcp.ws.api.exchange.util.AppConstants;

public abstract class ApiAbstractController {

	@Autowired
	protected MessageSource messageSource;
	
	@Autowired
	protected Environment environment;

	protected <T> ApiGenericResponse<T> buildApiResponse(HttpStatus code, String message) {
		ApiGenericResponse<T> apiGenericResponse = new ApiGenericResponse<T>();
		apiGenericResponse.setStatus(code);
		apiGenericResponse.setTimestamp(LocalDateTime.now());
		apiGenericResponse.setMessage(getValueMessage(message));
		return apiGenericResponse;
	}
	
	protected <T> ApiGenericResponse<T> buildApiResponse(HttpStatus code, String message, T data) {
		ApiGenericResponse<T> apiGenericResponse = new ApiGenericResponse<T>();
		apiGenericResponse.setStatus(code);
		apiGenericResponse.setTimestamp(LocalDateTime.now());
		apiGenericResponse.setMessage(getValueMessage(message));
		apiGenericResponse.setData(data);
		return apiGenericResponse;
	}
	
	protected <T> ApiGenericResponse<T> buildApiResponse(HttpStatus code, String message, Object[] parameters, T data) {
		ApiGenericResponse<T> apiGenericResponse = new ApiGenericResponse<T>();
		apiGenericResponse.setStatus(code);
		apiGenericResponse.setTimestamp(LocalDateTime.now());
		apiGenericResponse.setMessage(getValueMessage(message, parameters));
		apiGenericResponse.setData(data);
		return apiGenericResponse;
	}

	protected String getValueMessage(String key) {
		return messageSource.getMessage(key, new Object[] {}, AppConstants.NOT_FOUND_KEY, Locale.getDefault());
	}

	protected String getValueMessage(String key, Object[] parameters) {
		return messageSource.getMessage(key, parameters, AppConstants.NOT_FOUND_KEY, Locale.getDefault());
	}

	protected String getValueConfig(String key) {
		return environment.getProperty(key, AppConstants.NOT_FOUND_KEY);
	}
}
