package com.bcp.ws.api.exchange.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Data
public class UserDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	
	private String password;
	
	private String token;
	
}
