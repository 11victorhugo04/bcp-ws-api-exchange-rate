package com.bcp.ws.api.exchange.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bcp.ws.api.exchange.dto.ExchangeRateDTO;
import com.bcp.ws.api.exchange.entity.EntityExchangeRate;
import com.bcp.ws.api.exchange.exception.AppInternalException;
import com.bcp.ws.api.exchange.repository.ExchangeRateRepository;
import com.bcp.ws.api.exchange.service.ExchangeRateService;
import com.bcp.ws.api.exchange.util.AppMessages;

import rx.Single;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

	private ExchangeRateRepository exchangeRateRepository;

	private ModelMapper modelMapper;

	@Autowired
	public void setExchangeRateRepository(ExchangeRateRepository exchangeRateRepository) {
		this.exchangeRateRepository = exchangeRateRepository;
	}

	@Autowired
	public void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	@Override
	public Single<List<ExchangeRateDTO>> findAll() {
		return Single.create(subscriber -> {
			List<EntityExchangeRate> list = exchangeRateRepository.findAll();

			if (list.isEmpty())
				subscriber.onError(new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_LIST_EMPTY));
			else {
				List<ExchangeRateDTO> listDTO = list.stream()
						.map(e -> modelMapper.map(e, ExchangeRateDTO.class))
						.collect(Collectors.toList());

				subscriber.onSuccess(listDTO);
			}
		});
	}

	@Override
	public Single<ExchangeRateDTO> get(Long id) {
		return Single.create(subscriber -> {
			Optional<EntityExchangeRate> entityOptional = exchangeRateRepository.findById(id);

			if (!entityOptional.isPresent())
				subscriber.onError(new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_SINGLE_EMPTY));
			else {
				ExchangeRateDTO entityDTO = modelMapper.map(entityOptional.get(), ExchangeRateDTO.class);

				subscriber.onSuccess(entityDTO);
			}
		});
	}

	@Override
	public Single<ExchangeRateDTO> create(ExchangeRateDTO exchangeRateDTO) {
		return Single.create(singleSubscriber -> {
			if (exchangeRateDTO.getId() != null)
				singleSubscriber
						.onError(new AppInternalException(HttpStatus.BAD_REQUEST, AppMessages.GENERIC_SAVE_ERROR));
			else {
				EntityExchangeRate entity = modelMapper.map(exchangeRateDTO, EntityExchangeRate.class);
				entity = exchangeRateRepository.save(entity);
				ExchangeRateDTO entityDTO = modelMapper.map(entity, ExchangeRateDTO.class);

				singleSubscriber.onSuccess(entityDTO);
			}
		});
	}

	@Override
	public Single<ExchangeRateDTO> update(Long id, ExchangeRateDTO exchangeRateDTO) {
		exchangeRateDTO.setId(id);

		return Single.create(singleSubscriber -> {
			Optional<EntityExchangeRate> entityOptional = exchangeRateRepository.findById(exchangeRateDTO.getId());
			if (!entityOptional.isPresent())
				singleSubscriber
						.onError(new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_SINGLE_EMPTY));
			else {
				EntityExchangeRate entity= entityOptional.get();
				entity.setAmountExchangeRate(exchangeRateDTO.getAmountExchangeRate());
				
				entity = exchangeRateRepository.save(entityOptional.get());
				ExchangeRateDTO entityDTO = modelMapper.map(entity, ExchangeRateDTO.class);

				singleSubscriber.onSuccess(entityDTO);
			}
		});
	}

	@Override
	public Single<ExchangeRateDTO> calculateExchangeRate(ExchangeRateDTO exchangeRateDTO) {
		return Single.create(singleSubscriber -> {
			Optional<EntityExchangeRate> entityOptional = exchangeRateRepository.findByFromCurrencyAndToCurrency(exchangeRateDTO.getFromCurrency(), exchangeRateDTO.getToCurrency());
			if (!entityOptional.isPresent())
				singleSubscriber
						.onError(new AppInternalException(HttpStatus.BAD_REQUEST, AppMessages.NOT_EXIST_EXCHANGE_RATE, 
								Arrays.asList(exchangeRateDTO.getFromCurrency(), exchangeRateDTO.getToCurrency()).toArray()));
			else {
				ExchangeRateDTO entityDTO = modelMapper.map(entityOptional.get(), ExchangeRateDTO.class);
				
				exchangeRateDTO.setAmountExchangeRate(entityDTO.getAmountExchangeRate());
				exchangeRateDTO.setToAmount(exchangeRateDTO.getFromAmount().multiply(exchangeRateDTO.getAmountExchangeRate()));
				
				singleSubscriber.onSuccess(exchangeRateDTO);
			}
		});
	}
}
