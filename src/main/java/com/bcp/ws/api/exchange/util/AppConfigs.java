package com.bcp.ws.api.exchange.util;

public interface AppConfigs {

	String SPRING_APPLICATION_VERSION = "spring.application.version";
	String SPRING_PROFILE_ACTIVE = "spring.profiles.active";
}
