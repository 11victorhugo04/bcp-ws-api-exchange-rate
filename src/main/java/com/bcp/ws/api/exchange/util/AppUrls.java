package com.bcp.ws.api.exchange.util;

public interface AppUrls {

	String API_LOGIN = "/api/login";
	String API_LOGIN_USER = "/user";
	
	String API_EXCHANGE_RATE = "/api/exchange-rate";
	String API_EXCGANGE_RATE_LIST = "/list";
    String API_EXCGANGE_RATE_SINGLE = "/single";
    String API_EXCGANGE_RATE_SINGLE_ID = API_EXCGANGE_RATE_SINGLE + "/{id}";
    String API_EXCGANGE_RATE_CALCULATE = "/calculate";
    
    String H2_CONSOLE = "/h2-console/**";
}
