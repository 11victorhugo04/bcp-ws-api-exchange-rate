package com.bcp.ws.api.exchange.domain.error;

import java.io.Serializable;

import lombok.Data;

@Data
public class ApiErrorResponseDetail<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String type;
	
	private String code;
	
	private String description;
	
	private String message;
	
	private String object;
	
	private String field;
	
	private T data;
}
