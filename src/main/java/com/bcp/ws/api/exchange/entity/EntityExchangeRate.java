package com.bcp.ws.api.exchange.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.bcp.ws.api.exchange.util.AppConstants;

import lombok.Data;

@Data
@Entity
@Table(name = AppConstants.ENTITY_TABLE_EXCHANGE_RATE)
public class EntityExchangeRate {

	@Id
	@SequenceGenerator(name = "SEQUENCE_PK", sequenceName = "seq_exchange_rate", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_PK")
	private Long id;

	@NotBlank(message = "{fromCurrency.notempty}")
	private String fromCurrency;

	@NotBlank(message = "{toCurrency.notempty}")
	private String toCurrency;

	private BigDecimal amountExchangeRate;

}
