package com.bcp.ws.api.exchange.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.ws.api.exchange.dto.UserDTO;
import com.bcp.ws.api.exchange.exception.AppInternalException;
import com.bcp.ws.api.exchange.util.AppConstants;
import com.bcp.ws.api.exchange.util.AppMessages;
import com.bcp.ws.api.exchange.util.AppUrls;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RequestMapping(AppUrls.API_LOGIN)
@RestController
public class ApiWsLoginController extends ApiAbstractController {

	private final static String TOKEN_ID = "tokenJWT";
	private final static String PREFIX = "Bearer ";
	private final static String SECRET = "keyBCP";
	
	@PostMapping(AppUrls.API_LOGIN_USER)
	public UserDTO login(@RequestBody UserDTO userDTO) throws AppInternalException {
		
		if(!AppConstants.USERNAME_ADMIN.equals(userDTO.getUsername()) || 
				!AppConstants.PASSWORD_ADMIN.equals(userDTO.getPassword())) {
			throw new AppInternalException(HttpStatus.BAD_REQUEST, AppMessages.INVALID_CREDENTIALS);
		}
		
		String token = getJWTToken(userDTO.getUsername());
		userDTO.setToken(token);
		return userDTO;
	}

	private String getJWTToken(String username) {
		String secretKey = SECRET;
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder().setId(TOKEN_ID).setSubject(username)
				.claim(AppConstants.PARAM_AUTHORITIES, grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
		return PREFIX + token;
	}
}
