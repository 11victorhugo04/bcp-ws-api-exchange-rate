package com.bcp.ws.api.exchange.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcp.ws.api.exchange.entity.EntityExchangeRate;

@Repository
public interface ExchangeRateRepository extends JpaRepository<EntityExchangeRate, Long> {

	Optional<EntityExchangeRate> findByFromCurrencyAndToCurrency(String fromCurrency, String toCurrency);
	
}
