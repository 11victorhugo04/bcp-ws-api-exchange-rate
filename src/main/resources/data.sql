DROP TABLE IF EXISTS exchange_rate;
DROP SEQUENCE IF EXISTS seq_exchange_rate;

CREATE SEQUENCE seq_exchange_rate START WITH 1 INCREMENT BY 1;

CREATE TABLE exchange_rate (
  id INT DEFAULT seq_exchange_rate.nextval PRIMARY KEY,
  from_currency VARCHAR(250) NOT NULL,
  to_currency VARCHAR(250) NOT NULL,
  amount_exchange_rate DECIMAL(20,2) NOT NULL
);

INSERT INTO exchange_rate (from_currency, to_currency, amount_exchange_rate) VALUES
  ('PEN', 'USD', 3.1),
  ('USD', 'PEN', 3.5),
  ('EUR', 'PEN', 4.1),
  ('PEN', 'EUR', 4.5);